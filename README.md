# Pac-Man

This game project implements DFS, BFS, uniform cost search, and A* search to help Pac-Man find its target.  

![Pac-Man](/demo.png "Pac-Man")

## Requirements

`Python 3.6 or 3.7`
